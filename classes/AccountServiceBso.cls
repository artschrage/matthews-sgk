public with sharing class AccountServiceBso {

	public static final String WS_ERROR_SAVE_FAILED = 'Error: Save Failed';
	public static final String WS_ERROR_INVALID_PAYLOAD = 'Error: Invalid Webservice payload';
	public static final String WS_ERROR_NO_ACCOUNTS = 'Error: Unable to parse JSON for individual Account records';
	public static final String WS_ERROR_SEE_DATA_ERRORS='See Data Errors';
	public static final String WS_ERROR_EXCEEDS_RECORD_LIMIT='Error: Request contains more records than the max allowable (' + AccountService.MAX_RECORDS_PER_REQUEST + ')';
	public static final String WS_STATUS_FAILED = 'Failed';
	public static final String WS_STATUS_PROCESSED = 'Processed';
	public static final String WS_STATUS_PROCESSED_WITH_ERRORS = 'Processed With Errors';
	public static final String WS_DATA_UPDATE_SUCCESS = 'Success';
	public static final String WS_DATA_ERROR_MISSING_ID = 'Error: No Salesforce Id provided';
	public static final String WS_DATA_ERROR_RECORD_SKIPPED = 'Error: A problem exists with the record so it was not submitted for update in Salesforce';

	private AccountServiceDao dao;
	private AccountService.AccountUpdateRequest uReq;
	private AccountService.AccountUpdateResponse uResp;
	private Integer errorCount = 0;
	private Map<String, Id> mapParentCustomerNumberToId;

	public AccountServiceBso() {
		dao = new AccountServiceDao();
		mapParentCustomerNumberToId = new Map<String, Id>{};

	}

	public AccountService.AccountUpdateResponse processUpdateRequest(AccountService.AccountUpdateRequest uReqRaw) {
		uResp = null;

		String payloadValidationErrorMsg = validatePayload(uReqRaw);

		if (payloadValidationErrorMsg!='') {
			uResp = createWSResponseForError(WS_STATUS_FAILED, payloadValidationErrorMsg, null);

		} else {
			this.uReq = uReqRaw;

			Try {
				uResp = updateAccounts();

			} Catch(Exception e){
				uResp = createWSResponseForError(WS_STATUS_FAILED, WS_ERROR_SAVE_FAILED + ', ' + Utils.combineExceptionMessages(e), null);

			}

		}

		return uResp;
	}

	private String validatePayload(AccountService.AccountUpdateRequest uReqRaw) {
		String returnErrorMsgs = '';
		if (uReqRaw==null) {
			returnErrorMsgs = WS_ERROR_INVALID_PAYLOAD;

		} else {
			if(uReqRaw.accounts==null || uReqRaw.accounts.size()==0) {
				returnErrorMsgs = WS_ERROR_NO_ACCOUNTS;

			} else if (uReqRaw.accounts.size() > AccountService.MAX_RECORDS_PER_REQUEST) {
				returnErrorMsgs = WS_ERROR_EXCEEDS_RECORD_LIMIT;

			}

		}

		System.debug('*** payloadValidationErrorMsg: ' + returnErrorMsgs);
		return returnErrorMsgs;
	}

	public AccountService.AccountUpdateResponse createWSResponseForError(String status, String errorMessage, List<AccountService.AccountUpdateResult> updateResults) {
		AccountService.AccountUpdateResponse ur = new AccountService.AccountUpdateResponse();
		ur.dateTimeProcessed = Datetime.now();
		ur.status = status;
		ur.errorMessage = errorMessage;
		ur.updateResults = updateResults;
		return ur;
	}

	private AccountService.AccountUpdateResponse updateAccounts() {
		mapParentCustomerNumberToId = createMapParentCustomerNumberToId();

		List<Account> acctsToUpdate = createAccountObjsForUpdate();

		AccountServiceDao.DaoResults daoResults = dao.updateAccounts(acctsToUpdate);

		return createWSResponse(uReq, acctsToUpdate, daoResults);

	}

	private Map<String, Id> createMapParentCustomerNumberToId() {
		Map<String, Id> returnMap = new Map<String, Id>();

		Map<Id, Account> parentAccounts = dao.fetchAccountsByExternalId(createParentCustomerNumbersSet());

		for (Account a : parentAccounts.values()) {
			returnMap.put(a.External_Id__c, a.Id);
		}

		return returnMap;
	}

	private Set<String> createParentCustomerNumbersSet() {
		Set<String> returnSet = new Set<String>();

		if (uReq.accounts!=null) {
			for (AccountService.AccountRecord a : uReq.accounts) {
				if (a.parentCustomerNumber!=null) {
					returnSet.add(a.parentCustomerNumber);

				}

			}
		}

		return returnSet;
	}

	private List<Account> createAccountObjsForUpdate() {
		List<Account> returnList = new List<Account>();

		for (AccountService.AccountRecord a : uReq.accounts) {
			if (!String.isBlank(a.id)) {
				Account modAcct = new Account();
				modAcct.Id = a.id;
				if (!String.isBlank(a.name)) {
					modAcct.Name = a.name;
				}
				modAcct.ParentId = mapParentCustomerNumberToId.containsKey(a.parentCustomerNumber) ? mapParentCustomerNumberToId.get(a.parentCustomerNumber) : null;

				// Add additional fields for update here

				returnList.add(modAcct);
			}
		}

		return returnList;
	}

	private AccountService.AccountUpdateResponse createWSResponse(AccountService.AccountUpdateRequest uReq, List<Account> acctsToUpdate, AccountServiceDao.DaoResults daoResults) {
		AccountService.AccountUpdateResponse uResp = new AccountService.AccountUpdateResponse();

		List<AccountService.AccountUpdateResult> results = createAccountUpdateResults(uReq, acctsToUpdate, daoResults);

		uResp.dateTimeProcessed = Datetime.now();
		if (errorCount > 0) {
			uResp.status = WS_STATUS_PROCESSED_WITH_ERRORS;

		} else {
			uResp.status = WS_STATUS_PROCESSED;

		}
		uResp.updateResults = results;

		if (daoResults.dbException!=null) {
			uResp.errorMessage = Utils.combineExceptionMessages(daoResults.dbException);

		} else if (errorCount > 0) {
			uResp.errorMessage = WS_ERROR_SEE_DATA_ERRORS;

		}

		return uResp;

	}

	private List<AccountService.AccountUpdateResult> createAccountUpdateResults(AccountService.AccountUpdateRequest uReq, List<Account> acctsToUpdate, AccountServiceDao.DaoResults daoResults) {

		Map<Id, Database.SaveResult> mapIdToSaveResult = createMapIdToSaveResult(acctsToUpdate, daoResults);

		List<AccountService.AccountUpdateResult> results = new List<AccountService.AccountUpdateResult>{};

		errorCount = 0;

		if (uReq.accounts!=null) {
			for (Integer i=0; i < uReq.accounts.size(); i++) {
				AccountService.AccountUpdateResult newResult = new AccountService.AccountUpdateResult();

				if (uReq.accounts!=null) {
					AccountService.AccountRecord a = uReq.accounts[i];
					newResult.id = a.id;

					if (a.id!=null && mapIdToSaveResult.containsKey(a.id)) {
						Database.SaveResult sr = mapIdToSaveResult.get(a.id);

						if (sr.isSuccess()) {
							newResult.updateResult = WS_DATA_UPDATE_SUCCESS;

						} else {
							newResult.updateResult = Utils.combineErrorMessages(sr.getErrors());
							errorCount++;

						}

					} else {
						if (a.id!=null) {
							newResult.updateResult = WS_DATA_ERROR_RECORD_SKIPPED;

						} else {
							newResult.updateResult = WS_DATA_ERROR_MISSING_ID;

						}
					}
				}
				results.add(newResult);
			}
		}

		return results;

	}

	private Map<Id, Database.SaveResult> createMapIdToSaveResult(List<Account> acctsToUpdate, AccountServiceDao.DaoResults daoResults) {
		Map<Id, Database.SaveResult> returnMap = new Map<Id, Database.SaveResult>{};

		for (Integer i=0; i < acctsToUpdate.size(); i++) {
			Account a = acctsToUpdate[i];
			Database.SaveResult sr = daoResults.saveResults[i];
			if (a.Id!=null) {
				returnMap.put(a.Id, sr);

			}

		}

		Utils.logContents('mapIdToSaveResult: ', String.valueOf(returnMap));
		return returnMap;
	}

}