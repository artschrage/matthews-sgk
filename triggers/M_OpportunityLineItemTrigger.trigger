trigger M_OpportunityLineItemTrigger on OpportunityLineItem (after insert, after delete, after update) 
{
	if(Trigger.isInsert && Trigger.isAfter)
		M_OpportunityLineItemTriggerHandler.UpdateOpportunityBrands(Trigger.New);
	if(Trigger.isDelete && Trigger.isAfter)
		M_OpportunityLineItemTriggerHandler.UpdateOpportunityBrands(Trigger.Old);
	if(Trigger.isUpdate && Trigger.isAfter)
		M_OpportunityLineItemTriggerHandler.UpdateOpportunityBrands(Trigger.New);
}