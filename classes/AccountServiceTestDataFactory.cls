public with sharing class AccountServiceTestDataFactory {

	public AccountService.AccountUpdateRequest payload = null; 
	public Map<Id, Account> accounts = null;

	public AccountServiceTestDataFactory() {
		// Common elements for all test data setups
		
	}

	public void setupScenario1() {
		accounts = createGenericTestAccounts(2);
		createGenericTestPayload(accounts);

	}

	public void setupScenario2() {
		accounts = createGenericTestAccounts(1);
		createTestPayloadWithDataError(accounts);
		
	}

	public void setupScenario3() {
		accounts = createGenericTestAccounts(AccountService.MAX_RECORDS_PER_REQUEST + 1);
		createGenericTestPayload(accounts);
		
	}

	public void setupScenario4() {
		accounts = createGenericTestAccounts(2);
		createTestPayloadWithHierarchy(accounts.values()[0], accounts.values()[1]);

	}

	private Map<Id, Account> createGenericTestAccounts(Integer numAccounts) {
		List<Account> newAccounts = new List<Account>{};
		for (Integer i=0; i<numAccounts; i++) {
			newAccounts.add(new Account(Name='TestAccount' + i, External_Id__c='Test Account' + i));
		}
		insert newAccounts;
		return new Map<Id, Account>([Select Id, Name, External_Id__c From Account Where Id IN :newAccounts]);
	}

	private void createGenericTestPayload(Map<Id, Account> accts) {
		payload = new AccountService.AccountUpdateRequest();
		payload.accounts = createGenericTestPayloadAccountRecords(accts);

	}

	private void createTestPayloadWithDataError(Map<Id, Account> accts) {
		payload = new AccountService.AccountUpdateRequest();
		payload.accounts = createTestPayloadAccountRecordsWithDataErrors(accts);

	}

	private void createTestPayloadWithHierarchy(Account parentAcct, Account childAcct) {
		payload = new AccountService.AccountUpdateRequest();
		payload.accounts = createTestPayloadAccountRecordsWithHierarchy(parentAcct, childAcct);

	}

	private List<AccountService.AccountRecord> createGenericTestPayloadAccountRecords(Map<Id, Account> accts) {
		List<AccountService.AccountRecord> returnList = new List<AccountService.AccountRecord>{};

		for (Account a : accts.values()) {
			AccountService.AccountRecord newAccount = new AccountService.AccountRecord();
			newAccount.id = a.Id;
			newAccount.name = a.Name + 'meaningless name change';
			returnList.add(newAccount);

		}

		return returnList;
	}

	private List<AccountService.AccountRecord> createTestPayloadAccountRecordsWithHierarchy(Account parentAcct, Account childAcct) {
		List<AccountService.AccountRecord> returnList = new List<AccountService.AccountRecord>{};

		AccountService.AccountRecord newAccount = new AccountService.AccountRecord();
		newAccount.id = parentAcct.Id;
		newAccount.name = parentAcct.Name;
		newAccount.parentCustomerNumber = null;
		returnList.add(newAccount);

		newAccount = new AccountService.AccountRecord();
		newAccount.id = childAcct.Id;
		newAccount.name = childAcct.Name;
		newAccount.parentCustomerNumber = parentAcct.External_Id__c;
		returnList.add(newAccount);

		return returnList;
	}

	private List<AccountService.AccountRecord> createTestPayloadAccountRecordsWithDataErrors(Map<Id, Account> accts) {
		List<AccountService.AccountRecord> returnList = new List<AccountService.AccountRecord>{};

		for (Account a : accts.values()) {
			AccountService.AccountRecord newAccount = new AccountService.AccountRecord();
			newAccount.id = a.Id;
			newAccount.name = a.Name + 'Account Name too long - more than 255 characters 012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789';
			returnList.add(newAccount);

		}

		return returnList;
	}

}