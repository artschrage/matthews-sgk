public with sharing class AccountServiceDao {

	public Map<Id, Account> fetchAccountsByExternalId(Set<String> externalIds) {
		Map<Id, Account> returnMap = new Map<Id, Account>{};
		if (externalIds!=null && externalIds.size() > 0) {
			Try {
				returnMap = new Map<Id, Account>([Select Id, External_Id__c From Account Where External_Id__c IN :externalIds]);
				Utils.logContents('DB Query fetchAccountsByExternalId - Accounts found', String.valueOf(returnMap));
			} Catch (Exception e) {
				System.debug('*** DB Query fetchAccountsByExternalId - Error: ' + e.getMessage());
			}
		}
		return returnMap;
	}

	public DaoResults updateAccounts(List<Account> accts) {
		DaoResults daoResults = new DaoResults();

		Try {
			if (accts.size() > 0) {
	        	daoResults.saveResults = Database.update(accts, false);
				Utils.logContents('DB Saved updateAccounts', String.valueOf(accts));
				Utils.logContents('DB Saved updateAccounts - daoResults', String.valueOf(daoResults.saveResults));
	    	}
		} Catch(Exception e){
			daoResults.dbException = new DaoException(e.getMessage());
			daoResults.saveResults = null;
			System.debug(LoggingLevel.INFO, '*** DB Save updateAccounts - Error: ' + e.getMessage());
		}

		Utils.logContents('daoResults: ', String.valueOf(daoResults));
		return daoResults;
	}

	public class DaoResults {
		public DaoException dbException { get; set; }
		public Database.SaveResult[] saveResults { get; set; }

	}

	public class DaoException extends Exception {}

}