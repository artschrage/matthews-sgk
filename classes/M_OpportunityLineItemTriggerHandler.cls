public with sharing class M_OpportunityLineItemTriggerHandler 
{
    public static void UpdateOpportunityBrands(List<OpportunityLineItem> lineItems)
    {
        Set<Id> opportunityIds = new Set<Id>();

        for(OpportunityLineItem singleLineItem : lineItems)
            opportunityIds.add(singleLineItem.OpportunityId);

        (new M_OpportunityService()).AssignOpportunityCorrectBrands(opportunityIds);
    }
}