@isTest
private class M_OpportunityLineItemTrigger_CTest 
{
    private static String opportunityStage = '10-Mutual Interest Confirmed';
    private static testMethod void OpportunityLineItemTrigger_InsertTest()
    {
        Exception failureDuringExecution = null;

        Account testAccount = new Account(Name = 'Test Account 1');
        INSERT testAccount;
        Opportunity testOpportunity = new Opportunity(Name = 'Test Opportunity 1', AccountId = testAccount.Id, Type = 'Brand Delivery',
            LeadSource = 'Linkedin', CloseDate = System.now().date(), StageName = opportunityStage, Budget__c = 'Yes');
        INSERT testOpportunity;
        Product2 testProduct = new Product2(Name = 'Test Product 1');
        INSERT testProduct;
        PricebookEntry testPricebookEntry = new PriceBookEntry(Product2Id = testProduct.Id, PriceBook2Id = Test.getStandardPricebookid(),
            UnitPrice = 444.0, IsActive = true);
        INSERT testPricebookEntry;

        Test.startTest();
        
        try 
        { 
            INSERT new OpportunityLineItem(OpportunityId = testOpportunity.Id, True_Brand__c = 'Brandimage', Quantity = 10,  UnitPrice = 123,
                PricebookEntryId = testPricebookEntry.Id); 
        }
        catch(Exception failure) { failureDuringExecution = failure;  }

        Test.stopTest();

        Opportunity evaluatingOpportunity = [SELECT mkto71_Brand__c FROM Opportunity WHERE Id = :testOpportunity.Id];

        System.assertEquals('Brandimage', evaluatingOpportunity.mkto71_Brand__c, 'The brand insert failed!');
        System.assertEquals(null, failureDuringExecution, 'There was an exception thrown during the test!');
    }

    private static testMethod void OpportunityLineItemTrigger_DeleteTest()
    {
        Exception failureDuringExecution = null;

        Account testAccount = new Account(Name = 'Test Account 1');
        INSERT testAccount;
        Opportunity testOpportunity = new Opportunity(Name = 'Test Opportunity 1', AccountId = testAccount.Id, Type = 'Brand Delivery',
            LeadSource = 'Linkedin', CloseDate = System.now().date(), StageName = opportunityStage, Budget__c = 'Yes');
        INSERT testOpportunity;
        Product2 testProduct = new Product2(Name = 'Test Product 1');
        INSERT testProduct;
        PricebookEntry testPricebookEntry = new PriceBookEntry(Product2Id = testProduct.Id, PriceBook2Id = Test.getStandardPricebookid(),
            UnitPrice = 444.0, IsActive = true);
        INSERT testPricebookEntry;
        OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem(OpportunityId = testOpportunity.Id, True_Brand__c = 'Brandimage', 
            Quantity = 10,  UnitPrice = 123, PricebookEntryId = testPricebookEntry.Id);
        INSERT testOpportunityLineItem;

        Test.startTest();

        try { DELETE testOpportunityLineItem; }
        catch(Exception failure) { failureDuringExecution = failure;  }

        Test.stopTest();

        Opportunity evaluatingOpportunity = [SELECT mkto71_Brand__c FROM Opportunity WHERE Id = :testOpportunity.Id];

        System.assertEquals(null, evaluatingOpportunity.mkto71_Brand__c, 'The brand delete failed!');
        System.assertEquals(null, failureDuringExecution, 'There was an exception thrown during the test!');
    }

    private static testMethod void OpportunityLineItemTrigger_UpdateTest()
    {
        Exception failureDuringExecution = null;

        Account testAccount = new Account(Name = 'Test Account 1');
        INSERT testAccount;
        Opportunity testOpportunity = new Opportunity(Name = 'Test Opportunity 1', AccountId = testAccount.Id, Type = 'Brand Delivery',
            LeadSource = 'Linkedin', CloseDate = System.now().date(), StageName = opportunityStage, Budget__c = 'Yes');
        INSERT testOpportunity;
        Product2 testProduct = new Product2(Name = 'Test Product 1');
        INSERT testProduct;
        PricebookEntry testPricebookEntry = new PriceBookEntry(Product2Id = testProduct.Id, PriceBook2Id = Test.getStandardPricebookid(),
            UnitPrice = 444.0, IsActive = true);
        INSERT testPricebookEntry;
        OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem(OpportunityId = testOpportunity.Id, True_Brand__c = 'Brandimage', 
            Quantity = 10,  UnitPrice = 123, PricebookEntryId = testPricebookEntry.Id);
        INSERT testOpportunityLineItem;

        testOpportunityLineItem.True_Brand__c = 'Anthem';

        Test.startTest();

        try { UPDATE testOpportunityLineItem; }
        catch(Exception failure) { failureDuringExecution = failure;  }

        Test.stopTest();

        Opportunity evaluatingOpportunity = [SELECT mkto71_Brand__c FROM Opportunity WHERE Id = :testOpportunity.Id];

        System.assertEquals('Anthem', evaluatingOpportunity.mkto71_Brand__c, 'The brand update failed!');
        System.assertEquals(null, failureDuringExecution, 'There was an exception thrown during the test!');
    }

    private static testMethod void OpportunityLineItemTrigger_BatchTest()
    {
        Exception failureDuringExecution = null;

        Account testAccount = new Account(Name = 'Test Account 1');
        INSERT testAccount;
        Opportunity testOpportunity = new Opportunity(Name = 'Test Opportunity 1', AccountId = testAccount.Id, Type = 'Brand Delivery',
            LeadSource = 'Linkedin', CloseDate = System.now().date(), StageName = opportunityStage, Budget__c = 'Yes');
        INSERT testOpportunity;
        Product2 testProduct = new Product2(Name = 'Test Product 1');
        INSERT testProduct;
        PricebookEntry testPricebookEntry = new PriceBookEntry(Product2Id = testProduct.Id, PriceBook2Id = Test.getStandardPricebookid(),
            UnitPrice = 444.0, IsActive = true);
        INSERT testPricebookEntry;
        OpportunityLineItem testOpportunityLineItem = new OpportunityLineItem(OpportunityId = testOpportunity.Id, True_Brand__c = 'Brandimage', 
            Quantity = 10,  UnitPrice = 123, PricebookEntryId = testPricebookEntry.Id);
        INSERT testOpportunityLineItem;

        testOpportunity.mkto71_Brand__c = '';

        Test.startTest();
        
        try { Database.executeBatch(new M_OpportunityBrandCreationProcess()); }
        catch(Exception failure) { failureDuringExecution = failure;  }

        Test.stopTest();

        Opportunity evaluatingOpportunity = [SELECT mkto71_Brand__c FROM Opportunity WHERE Id = :testOpportunity.Id];

        System.assertEquals('Brandimage', evaluatingOpportunity.mkto71_Brand__c, 'The brand batch update failed!');
        System.assertEquals(null, failureDuringExecution, 'There was an exception thrown during the test!');
    }
}