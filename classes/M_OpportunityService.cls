public with sharing class M_OpportunityService 
{
    public void AssignOpportunityCorrectBrands(Set<Id> opportunityIds)
    {
        Map<Id, Set<String>> opportunityToBrandMap = new Map<Id, Set<String>>();

        for(OpportunityLineItem singleLineItem : [SELECT OpportunityId, True_Brand__c FROM OpportunityLineItem WHERE OpportunityId IN :opportunityIds])
        {
            if(!opportunityToBrandMap.containsKey(singleLineItem.OpportunityId))
                opportunityToBrandMap.put(singleLineItem.OpportunityId, new Set<String>());
            opportunityToBrandMap.get(singleLineItem.OpportunityId).add(singleLineItem.True_Brand__c);
        }

        List<Opportunity> opportunitiesToBrand = [SELECT Id FROM Opportunity WHERE Id IN :opportunityIds];

        for(Opportunity singleOpportunity : opportunitiesToBrand)
            singleOpportunity.mkto71_Brand__c = String.join(new List<String>(opportunityToBrandMap.get(singleOpportunity.Id)), ';');

        UPDATE opportunitiesToBrand;
    }
}