@isTest
private class AccountServiceTest
{
	static AccountServiceTestDataFactory testData;

	static void setup() {
		testData = new AccountServiceTestDataFactory();

	}

	@isTest
	static void testAccountServiceHappyPath()
	{
		setup();
		testData.setupScenario1();

		Test.startTest();	

		AccountService.AccountUpdateResponse responseObj = AccountService.updateAccounts(testdata.payload);

		Test.stopTest();

		Integer expected = testData.payload.accounts.size();
		String expectedStatus = AccountServiceBso.WS_STATUS_PROCESSED;
		String expectedResult0 = AccountServiceBso.WS_DATA_UPDATE_SUCCESS;

		System.assert(responseObj.updateResults.size()==expected, 'The AccountService did not return a result for every record');
		System.assert(responseObj.updateResults[1].updateResult==expectedResult0, 'The first record was not marked as Success');
		System.assert(responseObj.status==expectedStatus, 'The AccountService did not have a Processed status');
		System.assert(responseObj.errorMessage==null, 'The AccountService had an error at the top level');

	}

	@isTest
	static void testAccountServiceDataError()
	{
		setup();
		testData.setupScenario2();

		Test.startTest();

		AccountService.AccountUpdateResponse responseObj = AccountService.updateAccounts(testdata.payload);

		Test.stopTest();

		String expected1 = AccountServiceBso.WS_ERROR_SEE_DATA_ERRORS;
		String failResult2 = AccountServiceBso.WS_DATA_UPDATE_SUCCESS;

		System.assert(responseObj.updateResults[0].updateResult!=expected1, 'The AccountService returned no Error on the parent level when it should return a "See Data Errors" message');
		System.assert(responseObj.updateResults[0].updateResult!=failResult2, 'The AccountService returned data update Success when it should return a Data Error');

	}

	@isTest
	static void testAccountServiceExceedsMaxRecords()
	{
		setup();
		testData.setupScenario3();

		Test.startTest();	

		AccountService.AccountUpdateResponse responseObj = AccountService.updateAccounts(testdata.payload);

		Test.stopTest();

		String expectedError = AccountServiceBso.WS_ERROR_EXCEEDS_RECORD_LIMIT;

		System.assert(responseObj.errorMessage==expectedError, 'The AccountService did not show an Error for exceeding max record count');

	}

	@isTest
	static void testAccountServiceHierarchy()
	{
		setup();
		testData.setupScenario4();

		Test.startTest();	

		AccountService.AccountUpdateResponse responseObj = AccountService.updateAccounts(testdata.payload);

		Test.stopTest();

		Integer expected = 1;

		List<Account> childrenOfAcct0 = [Select Id From Account Where ParentId = :testData.accounts.values()[0].Id];

		System.assert(childrenOfAcct0.size()==expected, 'The AccountService did not create a parent-child relationship as expected');

	}

}