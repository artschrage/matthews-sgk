public with sharing class M_OpportunityBrandCreationProcess implements Database.Batchable<sObject>
{
    public Database.QueryLocator start(Database.BatchableContext batchableContextForBatch) 
    { 
        return Database.getQueryLocator('SELECT Id FROM Opportunity'); 
    }

    public void execute(Database.BatchableContext batchableContextForBatch, List<sObject> opportuntiesToBrand) 
    {
            (new M_OpportunityService()).AssignOpportunityCorrectBrands((new Map<Id, sObject>(opportuntiesToBrand)).keySet());
    }

    public void finish(Database.BatchableContext batchableContextForBatch) { }
}