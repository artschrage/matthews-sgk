@RestResource(urlMapping='/AccountService/*')
global class AccountService {

	public static final Integer MAX_RECORDS_PER_REQUEST = 200;

	@HttpPost
	global static AccountUpdateResponse updateAccounts(AccountUpdateRequest accountUpdateRequest) {

		AccountServiceBso bso = new AccountServiceBso();

		return bso.processUpdateRequest(accountUpdateRequest);

	}

	global class AccountUpdateRequest {
		public List<AccountRecord> accounts {get;set;}

	}

	global class AccountRecord {
		public String id {get;set;}
		public String name {get;set;}
		public String parentCustomerNumber {get;set;}

	}

	global class AccountUpdateResponse {
		public Datetime dateTimeProcessed { get; set; }
		public String status { get; set; }
		public String errorMessage { get; set; }
		public List<AccountUpdateResult> updateResults {get;set;}

	}

	global class AccountUpdateResult {
		public String id {get;set;}
		public String updateResult {get;set;}

	}

}